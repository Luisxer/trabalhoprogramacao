import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.JPasswordField;

public class Login {

	private JFrame frame;

	static final String JDBC_DRIVER = "org.sqlite.JDBC";
	static final String DB_URL = "jdbc:sqlite:bdCar.sqlite";
	static Connection conn = null;
	private JLabel lblMarca;
	private JLabel lblModelo;
	private JLabel lblMatricula;
	private JLabel lblCilindrada;
	private JLabel lblCombustivel;
	private JLabel lblAnoaquisicao;
	private JLabel lblPrecoaquisicao;
	private JLabel lblAnovenda;
	private JLabel lblValorvenda;
	private JTextField textModelo;
	private JTextField textMatricula;
	private JTextField textCilindrada;
	private JTextField textCombustivel;
	private JTextField textAnoAquisicao;
	private JTextField textPrecoAquisicao;
	private JTextField textAnoVenda;
	private JTextField textValorVenda;
	private JButton btnAdicionar;
	private JTable table;
	private JLabel label_27;
	private JScrollPane scrollPane;
	private JTextField textMarca;
	private JButton btnVerDespesas;
	private JTextField textCor;
	private JLabel lblCor;
	private JPanel panel;
	private JPasswordField passwordField;
	private JTextField textUser;
	private JPasswordField TxtPassword;
	public static int ID;

	public static int getID() {
		return ID;
	}

	public static void setID(int iD) {
		ID = iD;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	private void connectDB() {
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL);
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} // end try
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 401, 228);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JLabel lblUtilizador = new JLabel("Utilizador");
		GridBagConstraints gbc_lblUtilizador = new GridBagConstraints();
		gbc_lblUtilizador.insets = new Insets(0, 0, 5, 5);
		gbc_lblUtilizador.gridx = 2;
		gbc_lblUtilizador.gridy = 1;
		frame.getContentPane().add(lblUtilizador, gbc_lblUtilizador);
		
		textUser = new JTextField();
		GridBagConstraints gbc_textUser = new GridBagConstraints();
		gbc_textUser.insets = new Insets(0, 0, 5, 0);
		gbc_textUser.fill = GridBagConstraints.HORIZONTAL;
		gbc_textUser.gridx = 4;
		gbc_textUser.gridy = 1;
		frame.getContentPane().add(textUser, gbc_textUser);
		textUser.setColumns(10);
		
		JLabel lblPass = new JLabel("Pass");
		GridBagConstraints gbc_lblPass = new GridBagConstraints();
		gbc_lblPass.insets = new Insets(0, 0, 5, 5);
		gbc_lblPass.gridx = 2;
		gbc_lblPass.gridy = 2;
		frame.getContentPane().add(lblPass, gbc_lblPass);
		
		TxtPassword = new JPasswordField();
		TxtPassword.setEchoChar('?');
		GridBagConstraints gbc_TxtPassword = new GridBagConstraints();
		gbc_TxtPassword.insets = new Insets(0, 0, 5, 0);
		gbc_TxtPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_TxtPassword.gridx = 4;
		gbc_TxtPassword.gridy = 2;
		frame.getContentPane().add(TxtPassword, gbc_TxtPassword);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				CarTable table2 = new CarTable();
				table2.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnLogin = new GridBagConstraints();
		gbc_btnLogin.gridx = 4;
		gbc_btnLogin.gridy = 3;
		
		frame.getContentPane().add(btnLogin, gbc_btnLogin);
		
		
		connectDB();
		Dados.ReterDados(conn);
	
		DefaultComboBoxModel<String> CarModel = new DefaultComboBoxModel<String>();
		try {

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM Automovel");

			while (rs.next()) {
				// System.out.println(rs.getString("Marca"));
				CarModel.addElement(rs.getString("Marca"));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		/*
		textMarca = new JTextField();
		GridBagConstraints gbc_textMarca = new GridBagConstraints();
		gbc_textMarca.insets = new Insets(0, 0, 5, 5);
		gbc_textMarca.fill = GridBagConstraints.HORIZONTAL;
		gbc_textMarca.gridx = 0;
		gbc_textMarca.gridy = 2;
		frame.getContentPane().add(textMarca, gbc_textMarca);
		textMarca.setColumns(10);

		lblMarca = new JLabel("Marca");
		GridBagConstraints gbc_lblMarca = new GridBagConstraints();
		gbc_lblMarca.fill = GridBagConstraints.BOTH;
		gbc_lblMarca.insets = new Insets(0, 0, 5, 5);
		gbc_lblMarca.gridx = 2;
		gbc_lblMarca.gridy = 2;
		frame.getContentPane().add(lblMarca, gbc_lblMarca);

		btnVerDespesas = new JButton("Ver Despesas");
		GridBagConstraints gbc_btnVerDespesas = new GridBagConstraints();
		gbc_btnVerDespesas.insets = new Insets(0, 0, 5, 0);
		gbc_btnVerDespesas.gridx = 3;
		gbc_btnVerDespesas.gridy = 2;
		frame.getContentPane().add(btnVerDespesas, gbc_btnVerDespesas);

		textModelo = new JTextField();
		GridBagConstraints gbc_textModelo = new GridBagConstraints();
		gbc_textModelo.fill = GridBagConstraints.BOTH;
		gbc_textModelo.insets = new Insets(0, 0, 5, 5);
		gbc_textModelo.gridx = 0;
		gbc_textModelo.gridy = 3;
		frame.getContentPane().add(textModelo, gbc_textModelo);
		textModelo.setColumns(10);

		lblModelo = new JLabel("Modelo");
		GridBagConstraints gbc_lblModelo = new GridBagConstraints();
		gbc_lblModelo.fill = GridBagConstraints.BOTH;
		gbc_lblModelo.insets = new Insets(0, 0, 5, 5);
		gbc_lblModelo.gridx = 2;
		gbc_lblModelo.gridy = 3;
		frame.getContentPane().add(lblModelo, gbc_lblModelo);
		
		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 3;
		gbc_panel.gridy = 3;
		frame.getContentPane().add(panel, gbc_panel);

		textMatricula = new JTextField();
		GridBagConstraints gbc_textMatricula = new GridBagConstraints();
		gbc_textMatricula.fill = GridBagConstraints.BOTH;
		gbc_textMatricula.insets = new Insets(0, 0, 5, 5);
		gbc_textMatricula.gridx = 0;
		gbc_textMatricula.gridy = 4;
		frame.getContentPane().add(textMatricula, gbc_textMatricula);
		textMatricula.setColumns(10);

		lblMatricula = new JLabel("Matricula");
		GridBagConstraints gbc_lblMatricula = new GridBagConstraints();
		gbc_lblMatricula.fill = GridBagConstraints.BOTH;
		gbc_lblMatricula.insets = new Insets(0, 0, 5, 5);
		gbc_lblMatricula.gridx = 2;
		gbc_lblMatricula.gridy = 4;
		frame.getContentPane().add(lblMatricula, gbc_lblMatricula);

		textCilindrada = new JTextField();
		GridBagConstraints gbc_textCilindrada = new GridBagConstraints();
		gbc_textCilindrada.fill = GridBagConstraints.BOTH;
		gbc_textCilindrada.insets = new Insets(0, 0, 5, 5);
		gbc_textCilindrada.gridx = 0;
		gbc_textCilindrada.gridy = 5;
		frame.getContentPane().add(textCilindrada, gbc_textCilindrada);
		textCilindrada.setColumns(10);

		lblCilindrada = new JLabel("Cilindrada");
		GridBagConstraints gbc_lblCilindrada = new GridBagConstraints();
		gbc_lblCilindrada.fill = GridBagConstraints.BOTH;
		gbc_lblCilindrada.insets = new Insets(0, 0, 5, 5);
		gbc_lblCilindrada.gridx = 2;
		gbc_lblCilindrada.gridy = 5;
		frame.getContentPane().add(lblCilindrada, gbc_lblCilindrada);

		textCombustivel = new JTextField();
		GridBagConstraints gbc_textCombustivel = new GridBagConstraints();
		gbc_textCombustivel.fill = GridBagConstraints.BOTH;
		gbc_textCombustivel.insets = new Insets(0, 0, 5, 5);
		gbc_textCombustivel.gridx = 0;
		gbc_textCombustivel.gridy = 6;
		frame.getContentPane().add(textCombustivel, gbc_textCombustivel);
		textCombustivel.setColumns(10);

		lblCombustivel = new JLabel("Combustivel");
		GridBagConstraints gbc_lblCombustivel = new GridBagConstraints();
		gbc_lblCombustivel.fill = GridBagConstraints.BOTH;
		gbc_lblCombustivel.insets = new Insets(0, 0, 5, 5);
		gbc_lblCombustivel.gridx = 2;
		gbc_lblCombustivel.gridy = 6;
		frame.getContentPane().add(lblCombustivel, gbc_lblCombustivel);

		textAnoAquisicao = new JTextField();
		GridBagConstraints gbc_textAnoAquisicao = new GridBagConstraints();
		gbc_textAnoAquisicao.fill = GridBagConstraints.BOTH;
		gbc_textAnoAquisicao.insets = new Insets(0, 0, 5, 5);
		gbc_textAnoAquisicao.gridx = 0;
		gbc_textAnoAquisicao.gridy = 7;
		frame.getContentPane().add(textAnoAquisicao, gbc_textAnoAquisicao);
		textAnoAquisicao.setColumns(10);

		lblAnoaquisicao = new JLabel("anoAquisicao");
		GridBagConstraints gbc_lblAnoaquisicao = new GridBagConstraints();
		gbc_lblAnoaquisicao.fill = GridBagConstraints.BOTH;
		gbc_lblAnoaquisicao.insets = new Insets(0, 0, 5, 5);
		gbc_lblAnoaquisicao.gridx = 2;
		gbc_lblAnoaquisicao.gridy = 7;
		frame.getContentPane().add(lblAnoaquisicao, gbc_lblAnoaquisicao);

		textPrecoAquisicao = new JTextField();
		GridBagConstraints gbc_textPrecoAquisicao = new GridBagConstraints();
		gbc_textPrecoAquisicao.fill = GridBagConstraints.BOTH;
		gbc_textPrecoAquisicao.insets = new Insets(0, 0, 5, 5);
		gbc_textPrecoAquisicao.gridx = 0;
		gbc_textPrecoAquisicao.gridy = 8;
		frame.getContentPane().add(textPrecoAquisicao, gbc_textPrecoAquisicao);
		textPrecoAquisicao.setColumns(10);

		lblPrecoaquisicao = new JLabel("pre\u00E7oAquisi\u00E7ao");
		GridBagConstraints gbc_lblPrecoaquisicao = new GridBagConstraints();
		gbc_lblPrecoaquisicao.fill = GridBagConstraints.BOTH;
		gbc_lblPrecoaquisicao.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrecoaquisicao.gridx = 2;
		gbc_lblPrecoaquisicao.gridy = 8;
		frame.getContentPane().add(lblPrecoaquisicao, gbc_lblPrecoaquisicao);

		textAnoVenda = new JTextField();
		GridBagConstraints gbc_textAnoVenda = new GridBagConstraints();
		gbc_textAnoVenda.fill = GridBagConstraints.BOTH;
		gbc_textAnoVenda.insets = new Insets(0, 0, 5, 5);
		gbc_textAnoVenda.gridx = 0;
		gbc_textAnoVenda.gridy = 9;
		frame.getContentPane().add(textAnoVenda, gbc_textAnoVenda);
		textAnoVenda.setColumns(10);

		lblAnovenda = new JLabel("AnoVenda");
		GridBagConstraints gbc_lblAnovenda = new GridBagConstraints();
		gbc_lblAnovenda.fill = GridBagConstraints.BOTH;
		gbc_lblAnovenda.insets = new Insets(0, 0, 5, 5);
		gbc_lblAnovenda.gridx = 2;
		gbc_lblAnovenda.gridy = 9;
		frame.getContentPane().add(lblAnovenda, gbc_lblAnovenda);

		textValorVenda = new JTextField();
		GridBagConstraints gbc_textValorVenda = new GridBagConstraints();
		gbc_textValorVenda.fill = GridBagConstraints.BOTH;
		gbc_textValorVenda.insets = new Insets(0, 0, 5, 5);
		gbc_textValorVenda.gridx = 0;
		gbc_textValorVenda.gridy = 10;
		frame.getContentPane().add(textValorVenda, gbc_textValorVenda);
		textValorVenda.setColumns(10);

		lblValorvenda = new JLabel("ValorVenda");
		GridBagConstraints gbc_lblValorvenda = new GridBagConstraints();
		gbc_lblValorvenda.fill = GridBagConstraints.BOTH;
		gbc_lblValorvenda.insets = new Insets(0, 0, 5, 5);
		gbc_lblValorvenda.gridx = 2;
		gbc_lblValorvenda.gridy = 10;
		frame.getContentPane().add(lblValorvenda, gbc_lblValorvenda);

		textCor = new JTextField();
		GridBagConstraints gbc_textCor = new GridBagConstraints();
		gbc_textCor.insets = new Insets(0, 0, 5, 5);
		gbc_textCor.fill = GridBagConstraints.HORIZONTAL;
		gbc_textCor.gridx = 1;
		gbc_textCor.gridy = 11;
		frame.getContentPane().add(textCor, gbc_textCor);
		textCor.setColumns(10);

		lblCor = new JLabel("Cor");
		GridBagConstraints gbc_lblCor = new GridBagConstraints();
		gbc_lblCor.insets = new Insets(0, 0, 5, 5);
		gbc_lblCor.gridx = 2;
		gbc_lblCor.gridy = 11;
		frame.getContentPane().add(lblCor, gbc_lblCor);

		label_27 = new JLabel("");
		GridBagConstraints gbc_label_27 = new GridBagConstraints();
		gbc_label_27.fill = GridBagConstraints.BOTH;
		gbc_label_27.insets = new Insets(0, 0, 5, 5);
		gbc_label_27.gridx = 2;
		gbc_label_27.gridy = 12;
		frame.getContentPane().add(label_27, gbc_label_27);

		btnAdicionar = new JButton("Adicionar");
		btnAdicionar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				System.out.println("Opened database successfully");
				Automovel v = new Automovel (99,textMarca.getText(),textModelo.getText(),textCor.getText(),textMatricula.getText(),Double.parseDouble(textCilindrada.getText()),textCombustivel.getText(), Integer.parseInt(textAnoAquisicao.getText()),Double.parseDouble(textPrecoAquisicao.getText()),Integer.parseInt(textAnoVenda.getText()),Double.parseDouble(textValorVenda.getText()));
				Dados.gravaBD(v);
			}



			});


		GridBagConstraints gbc_btnAdicionar = new GridBagConstraints();
		gbc_btnAdicionar.fill = GridBagConstraints.BOTH;
		gbc_btnAdicionar.insets = new Insets(0, 0, 5, 0);
		gbc_btnAdicionar.gridx = 3;
		gbc_btnAdicionar.gridy = 12;
		frame.getContentPane().add(btnAdicionar, gbc_btnAdicionar);
		GridBagConstraints gbc_btnLoad_1_1 = new GridBagConstraints();
		gbc_btnLoad_1_1.fill = GridBagConstraints.BOTH;
		gbc_btnLoad_1_1.insets = new Insets(0, 0, 5, 0);
		gbc_btnLoad_1_1.gridx = 0;
		gbc_btnLoad_1_1.gridy = 0;
		*/

		DefaultListModel<String> listmodel = new DefaultListModel<String>();
		}
	}
	/*try {

System.out.println("Opened database successfully");

Statement stmt = conn.createStatement();
String sql = "INSERT INTO AUTOMOVEL ("+ textMarca+","+textModelo+",COR,ADDRESS,SALARY) " +
		"VALUES (1, 'Paul', 32, 'California', 20000.00 );"; 
stmt.executeUpdate(sql);
stmt.close();
conn.close();
} catch ( Exception e ) {
System.err.println( e.getClass().getName() + ": " + e.getMessage() );
System.exit(0);
}
System.out.println("Carro Inserido");
	 */