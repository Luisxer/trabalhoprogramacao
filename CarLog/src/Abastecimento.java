
public class Abastecimento extends Despesa {
	
	
	private String local;
	private String kms;
	private double litros;
	private int idAbastecimento;
	public Abastecimento(int idAbastecimento, int valor, String data, String local, String kms, double litros,int idCarro) {
		super(valor, data, idCarro);
		// TODO Auto-generated constructor stub
	}

	public String getLocal() {
		return local;
	}

	public String getKms() {
		return kms;
	}

	public double getLitros() {
		return litros;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public void setKms(String kms) {
		this.kms = kms;
	}

	public void setLitros(double litros) {
		this.litros = litros;
	}

	public int getIdAbastecimento() {
		return idAbastecimento;
	}

	public void setIdAbastecimento(int idAbastecimento) {
		this.idAbastecimento = idAbastecimento;
	}
	

}
