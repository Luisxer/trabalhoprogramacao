/**
 * 
 */

/**
 * @author a54048
 *
 */
public class Revisao extends Despesa {
	
	
	private String oficina;
	private double kms;
	private String observacoes;
	private int idRevisao;
	/**
	 * @param valor
	 * @param data
	 */
	public Revisao(int idRevisao, int valor, String data, String oficina, double kms, String observacoes,int idCarro) {
		super(valor, data, idCarro);
		// TODO Auto-generated constructor stub
		this.oficina = oficina;
		this.kms = kms;
		this.observacoes = observacoes;
	}

	/**
	 * @return the oficina
	 */
	public String getOficina() {
		return oficina;
	}

	/**
	 * @return the kms
	 */
	public double getKms() {
		return kms;
	}

	/**
	 * @return the observacoes
	 */
	public String getObservacoes() {
		return observacoes;
	}

	/**
	 * @param oficina the oficina to set
	 */
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	/**
	 * @param kms the kms to set
	 */
	public void setKms(double kms) {
		this.kms = kms;
	}

	/**
	 * @param observacoes the observacoes to set
	 */
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	@Override
	public String toString() {
		return "Revisao [oficina=" + oficina + ", kms=" + kms + ", observacoes=" + observacoes + "]";
	}
	
	
	

}
