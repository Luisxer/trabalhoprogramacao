import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;

import javax.swing.JList;

public class Dados {
	static final String JDBC_DRIVER = "org.sqlite.JDBC";
	static final String DB_URL = "jdbc:sqlite:bdCar.sqlite";
	static Connection conn = null;

	private static int idCarro;
	private static String marca;
	private static String modelo;
	private static String cor;
	private static String matricula;
	private static double cilindrada;
	private static String combustivel;
	private static int anoAquisicao;
	private static double precoAquisicao;
	private static int anoVenda;
	private static double valorVenda;

	private void connDB() {

		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL);
		} catch (

		SQLException se)

		{
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (

		Exception e)

		{
			// Handle errors for Class.forName
			e.printStackTrace();
		} // end try

	}

	public Dados() {
		connDB();

	}

	public static void ReterDados(Connection conn) {
		// TODO Auto-generated method stub
		try {
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM Automovel ");
			Vector<Automovel> carro = new Vector<Automovel>();

			while (rs.next()) {
				idCarro = rs.getInt("idCarro");
				marca = rs.getString("marca");
				modelo = rs.getString("modelo");
				cor = rs.getString("cor");
				matricula = rs.getString("matricula");
				cilindrada = rs.getDouble("cilindrada");
				combustivel = rs.getString("combustivel");
				anoAquisicao = rs.getInt("anoAquisicao");
				precoAquisicao = rs.getDouble("precoAquisicao");
				anoVenda = rs.getInt("anoVenda");
				valorVenda = rs.getInt("valorVenda");
				carro.add(new Automovel(idCarro, marca, modelo, cor, matricula, cilindrada, combustivel, anoAquisicao,
						precoAquisicao, anoVenda, valorVenda));
			}
			;
			
		} catch (

		Exception e)

		{
			System.out.println(e.getMessage());

		}
	}

	public static void gravaBD(Automovel v) {
			
			try {
				String sql = "INSERT INTO AUTOMOVEL (marca, modelo, cor, matricula, cilindrada, combustivel, anoAquisicao, precoAquisicao, anoVenda,valorVenda)"
						+ " VALUES ('?','?','?','?',?,'?',?,?,?,?)";
				PreparedStatement pst = conn.prepareStatement(sql);
				pst.setString(1, v.getMarca());
				pst.setString(2, v.getModelo());
				pst.setString(3, v.getCor());
				pst.setString(4, v.getMatricula());
				pst.setDouble(5, v.getCilindrada());
				pst.setString(1, v.getCombustivel());
				pst.setInt(7, v.getAnoAquisicao());
				pst.setDouble(8, v.getPrecoAquisicao());
				pst.setInt(9, v.getAnoVenda());
				pst.setDouble(10, v.getValorVenda());
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/*
			 * }else{ String sql =
			 * "update INTO AUTOMOVEL (marca, modelo, cor, matricula, cilindrada, combustivel, anoAquisicao, precoAquisicao, anoVenda,valorVenda)"
			 * + where idCaroo = �kl }
			 */

	}
}
