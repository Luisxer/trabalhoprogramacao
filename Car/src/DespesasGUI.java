import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class DespesasGUI extends JFrame {

	static final String JDBC_DRIVER = "org.sqlite.JDBC";
	static final String DB_URL = "jdbc:sqlite:bdCar.sqlite";
	static Connection conn = null;
	
	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DespesasGUI frame = new DespesasGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	private void connectDB() {
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL);
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} // end try
	}

	/**
	 * Create the frame.
	 */
	public DespesasGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		connectDB();
		Dados.ReterDados(conn);
		
		String[] columnNames = { "tipo de despesa", "valor", "data", "idCarro" };
		Object[][] data = {};
		DefaultTableModel modelT = new DefaultTableModel(data, columnNames);

		try {
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM Imposto where idCarro="+Login.getID());

			while (rs.next()) {
				Object[] row = { "imposto",rs.getString("valor"), rs.getString("data"),
						rs.getString("idCarro") };

				modelT.addRow(row);

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());

		}
		try {
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM Abastecimento where idCarro="+Login.getID());

			while (rs.next()) {
				Object[] row = { "abastecimento",rs.getString("valor"), rs.getString("data"),
						rs.getString("idCarro") };

				modelT.addRow(row);

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());

		}
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(28, 25, 396, 141);
		contentPane.add(scrollPane);
		
		table = new JTable(modelT);
		scrollPane.setViewportView(table);
		
		
		
		
	}
}
