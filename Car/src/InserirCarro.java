import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class InserirCarro extends JFrame {
	
	static final String JDBC_DRIVER = "org.sqlite.JDBC";
	static final String DB_URL = "jdbc:sqlite:bdCar.sqlite";
	static Connection conn = null;
	
	


	private JPanel contentPane;
	private JTextField textMarca;
	private JTextField textModelo;
	private JTextField textCor;
	private JTextField textMatricula;
	private JTextField textCilindrada;
	private JTextField textCombustivel;
	private JTextField textAnoAquisicao;
	private JTextField textPrecoAquisicao;
	private JTextField textAnoVenda;
	private JTextField textValorVenda;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InserirCarro frame = new InserirCarro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void connectDB() {
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL);
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} // end try
	}

	/**
	 * Create the frame.
	 */
	public InserirCarro() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 485, 369);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setBounds(10, 50, 46, 14);
		contentPane.add(lblMarca);

		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(10, 75, 46, 14);
		contentPane.add(lblModelo);

		JLabel lblCor = new JLabel("Cor");
		lblCor.setBounds(10, 100, 46, 14);
		contentPane.add(lblCor);

		JLabel lblMatricula = new JLabel("Matricula");
		lblMatricula.setBounds(10, 125, 88, 14);
		contentPane.add(lblMatricula);

		JLabel lblCilindrada = new JLabel("Cilindrada");
		lblCilindrada.setBounds(10, 143, 70, 14);
		contentPane.add(lblCilindrada);

		JLabel lblCombustivel = new JLabel("Combustivel");
		lblCombustivel.setBounds(10, 168, 70, 14);
		contentPane.add(lblCombustivel);

		JLabel lblAnoAquisicao = new JLabel("Ano Aquisi\u00E7\u00E3o");
		lblAnoAquisicao.setBounds(10, 193, 88, 14);
		contentPane.add(lblAnoAquisicao);

		JLabel lblPrecoAquisicao = new JLabel("preco Aquisicao");
		lblPrecoAquisicao.setBounds(10, 218, 88, 14);
		contentPane.add(lblPrecoAquisicao);

		JLabel lblAnoVenda = new JLabel("ano Venda");
		lblAnoVenda.setBounds(10, 243, 88, 14);
		contentPane.add(lblAnoVenda);

		JLabel lblValorVenda = new JLabel("valor Venda");
		lblValorVenda.setBounds(10, 268, 88, 14);
		contentPane.add(lblValorVenda);

		textMarca = new JTextField();
		textMarca.setBounds(152, 47, 86, 20);
		contentPane.add(textMarca);
		textMarca.setColumns(10);

		textModelo = new JTextField();
		textModelo.setBounds(152, 72, 86, 20);
		contentPane.add(textModelo);
		textModelo.setColumns(10);

		textCor = new JTextField();
		textCor.setBounds(152, 97, 86, 20);
		contentPane.add(textCor);
		textCor.setColumns(10);

		textMatricula = new JTextField();
		textMatricula.setBounds(152, 122, 86, 14);
		contentPane.add(textMatricula);
		textMatricula.setColumns(10);

		textCilindrada = new JTextField();
		textCilindrada.setBounds(152, 140, 86, 20);
		contentPane.add(textCilindrada);
		textCilindrada.setColumns(10);

		textCombustivel = new JTextField();
		textCombustivel.setBounds(152, 165, 86, 20);
		contentPane.add(textCombustivel);
		textCombustivel.setColumns(10);

		textAnoAquisicao = new JTextField();
		textAnoAquisicao.setBounds(152, 190, 86, 20);
		contentPane.add(textAnoAquisicao);
		textAnoAquisicao.setColumns(10);

		textPrecoAquisicao = new JTextField();
		textPrecoAquisicao.setBounds(152, 215, 86, 20);
		contentPane.add(textPrecoAquisicao);
		textPrecoAquisicao.setColumns(10);

		textAnoVenda = new JTextField();
		textAnoVenda.setBounds(152, 240, 86, 20);
		contentPane.add(textAnoVenda);
		textAnoVenda.setColumns(10);

		textValorVenda = new JTextField();
		textValorVenda.setBounds(152, 265, 86, 20);
		contentPane.add(textValorVenda);
		textValorVenda.setColumns(10);

		JButton btnInserircarro = new JButton("InserirCarro");
		btnInserircarro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Automovel v = new Automovel(99, textMarca.getText(), textModelo.getText(), textCor.getText(),
						textMatricula.getText(), Double.parseDouble(textCilindrada.getText()),
						textCombustivel.getText(), Integer.parseInt(textAnoAquisicao.getText()),
						Double.parseDouble(textPrecoAquisicao.getText()), Integer.parseInt(textAnoVenda.getText()),
						Double.parseDouble(textValorVenda.getText()));
				/*try {
					String sql = "INSERT INTO AUTOMOVEL (marca, modelo, cor, matricula, cilindrada, combustivel, anoAquisicao, precoAquisicao, anoVenda,valorVenda)"
							+ " VALUES ('?','?','?','?',?,'?',?,?,?,?)";
					PreparedStatement pst = conn.prepareStatement(sql);
					pst.setString(1, v.getMarca());
					pst.setString(2, v.getModelo());
					pst.setString(3, v.getCor());
					pst.setString(4, v.getMatricula());
					pst.setDouble(5, v.getCilindrada());
					pst.setString(1, v.getCombustivel());
					pst.setInt(7, v.getAnoAquisicao());
					pst.setDouble(8, v.getPrecoAquisicao());
					pst.setInt(9, v.getAnoVenda());
					pst.setDouble(10, v.getValorVenda());
					ResultSet rs =pst.executeQuery();
					pst.close();
					rs.close();
					
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				contentPane.setVisible(false);
				//CarTable table = new CarTable();
				//table.setVisible(true);
				
				
			}
		});
		btnInserircarro.setBounds(149, 296, 103, 23);
		contentPane.add(btnInserircarro);
	}
}
