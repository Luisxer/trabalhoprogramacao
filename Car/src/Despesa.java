/**
 * 
 */

/**
 * @author a54048
 *
 */
public abstract class Despesa  {
	private int valor;
	private String data;
	private int idCarro;
	
	/**
	 * @param matricula
	 * @param valor
	 * @param data
	 */
	public Despesa(int valor, String data,int idCarro) {
		super();
		this.valor = valor;
		this.data = data;
	}

	public int getValor() {
		return valor;
	}

	public String getData() {
		return data;
	}
	
	public void setValor(int valor) {
		this.valor = valor;
	}

	public void setData(String data) {
		this.data = data;
	}




	
	
}
