import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CarTable extends JFrame {

	private JPanel contentPane;
	static final String JDBC_DRIVER = "org.sqlite.JDBC";
	static final String DB_URL = "jdbc:sqlite:bdCar.sqlite";
	static Connection conn = null;
	private JTextField textField;
	private JButton btnVerDespesas;
	private JButton btnVerDetalhes;
	private JTable table;
	private JScrollPane scrollPane;
	private JButton btnInserirCarro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CarTable frame = new CarTable();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void connectDB() {
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL);
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} // end try
	}

	/**
	 * Create the frame.
	 */
	public CarTable() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0 };
		gbl_contentPane.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);
		connectDB();
		Dados.ReterDados(conn);
		
		String[] columnNames = { "id", "marca", "modelo", "cor", "matricula", "cilindrada", "valorVenda" };
		Object[][] data = {};
		DefaultTableModel modelT = new DefaultTableModel(data, columnNames);

		try {
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM Automovel ");

			while (rs.next()) {
				Object[] row = { rs.getString("idCarro"), rs.getString("marca"), rs.getString("modelo"),
						rs.getString("cor"), rs.getString("matricula"), rs.getString("cilindrada"),
						rs.getString("valorVenda") };

				modelT.addRow(row);

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());

		}
				
				scrollPane = new JScrollPane();
				GridBagConstraints gbc_scrollPane = new GridBagConstraints();
				gbc_scrollPane.fill = GridBagConstraints.BOTH;
				gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
				gbc_scrollPane.gridx = 0;
				gbc_scrollPane.gridy = 0;
				contentPane.add(scrollPane, gbc_scrollPane);
				
				table = new JTable(modelT);
				scrollPane.setViewportView(table);
				
				textField = new JTextField();
				GridBagConstraints gbc_textField = new GridBagConstraints();
				gbc_textField.insets = new Insets(0, 0, 5, 0);
				gbc_textField.fill = GridBagConstraints.HORIZONTAL;
				gbc_textField.gridx = 0;
				gbc_textField.gridy = 1;
				contentPane.add(textField, gbc_textField);
				textField.setColumns(10);
				
				btnVerDespesas = new JButton("Ver Despesas");
				btnVerDespesas.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Login.setID(Integer.parseInt(textField.getText()));
						contentPane.setVisible(false);
						DespesasGUI despesas = new DespesasGUI();
						despesas.setVisible(true);
					}
				});
				GridBagConstraints gbc_btnVerDespesas = new GridBagConstraints();
				gbc_btnVerDespesas.insets = new Insets(0, 0, 5, 0);
				gbc_btnVerDespesas.gridx = 0;
				gbc_btnVerDespesas.gridy = 2;
				contentPane.add(btnVerDespesas, gbc_btnVerDespesas);
				
				btnVerDetalhes = new JButton("Ver Detalhes");
				GridBagConstraints gbc_btnVerDetalhes = new GridBagConstraints();
				gbc_btnVerDetalhes.insets = new Insets(0, 0, 5, 0);
				gbc_btnVerDetalhes.gridx = 0;
				gbc_btnVerDetalhes.gridy = 3;
				contentPane.add(btnVerDetalhes, gbc_btnVerDetalhes);
				
				btnInserirCarro = new JButton("Inserir Carro");
				btnInserirCarro.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						contentPane.setVisible(false);
						InserirCarro insC = new InserirCarro();
						insC.setVisible(true);
					}
				});
				GridBagConstraints gbc_btnInserirCarro = new GridBagConstraints();
				gbc_btnInserirCarro.gridx = 0;
				gbc_btnInserirCarro.gridy = 4;
				contentPane.add(btnInserirCarro, gbc_btnInserirCarro);

	}
}