
/**
 * @author a54048
 *
 */
public class Automovel {
	private int idCarro;
	private String marca;
	private String modelo;
	private String cor;
	private String matricula;
	private double cilindrada;
	private String combustivel;
	private int anoAquisicao;
	private double precoAquisicao;
	private int anoVenda;
	private double valorVenda;
	
	/**
	 * @param marca
	 * @param modelo
	 * @param cor
	 * @param matricula
	 * @param cilindrada
	 * @param combustivel
	 * @param anoAquisicao
	 * @param precoAquisicao
	 * @param anoVenda
	 * @param valorVenda
	 * @details Class Automovel
	 */
	public Automovel(int idCarro, String marca, String modelo, String cor, String matricula, double cilindrada, 
			String combustivel, int anoAquisicao, double precoAquisicao, int anoVenda, double valorVenda){
		this.marca = marca;
		this.modelo = modelo;
		this.cor = cor;
		this.matricula = matricula;
		this.cilindrada = cilindrada;
		this.combustivel = combustivel;
		this.anoAquisicao = anoAquisicao;
		this.precoAquisicao = precoAquisicao;
		this.anoVenda = anoVenda;
		this.valorVenda = valorVenda;
	}

	public String getMarca() {
		return marca;
	}

	public String getModelo() {
		return modelo;
	}

	public String getCor() {
		return cor;
	}

	public String getMatricula() {
		return matricula;
	}

	public double getCilindrada() {
		return cilindrada;
	}

	public String getCombustivel() {
		return combustivel;
	}

	public int getAnoAquisicao() {
		return anoAquisicao;
	}

	public double getPrecoAquisicao() {
		return precoAquisicao;
	}

	public int getAnoVenda() {
		return anoVenda;
	}

	public double getValorVenda() {
		return valorVenda;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public void setCilindrada(double cilindrada) {
		this.cilindrada = cilindrada;
	}

	public void setCombustivel(String combustivel) {
		this.combustivel = combustivel;
	}

	public void setAnoAquisicao(int anoAquisicao) {
		this.anoAquisicao = anoAquisicao;
	}

	public void setPrecoAquisicao(double precoAquisicao) {
		this.precoAquisicao = precoAquisicao;
	}

	public void setAnoVenda(int anoVenda) {
		this.anoVenda = anoVenda;
	}

	public void setValorVenda(double valorVenda) {
		this.valorVenda = valorVenda;
	}

	@Override
	public String toString() {
		return "Automovel [marca=" + marca + ", modelo=" + modelo + ", cor=" + cor + ", matricula=" + matricula
				+ ", cilindrada=" + cilindrada + ", combustivel=" + combustivel + ", anoAquisicao=" + anoAquisicao
				+ ", precoAquisicao=" + precoAquisicao + ", anoVenda=" + anoVenda + ", valorVenda=" + valorVenda + "]";
	}

	public int getIdCarro() {
		return idCarro;
	}

	public void setIdCarro(int idCarro) {
		this.idCarro = idCarro;
	}
	
	
	
}
